package controllers

import (
	"log"
	"time"

	"bitbucket.org/iminsoft/employee-cli/model"
	"bitbucket.org/iminsoft/front/caller"
	"github.com/kataras/iris"
	"github.com/valyala/fasthttp"
)

func HandleLogin(c *iris.Context) {
	if c.IsPost() {
		login := string(c.FormValue("login"))
		password := string(c.FormValue("password"))
		domain := string(c.FormValue("domain"))

		if response, err := caller.Login(login, password, domain); err != nil {
			log.Printf("Got an error logging in: %s", err.Error())
			getLoginPage(c, err.Error())
		} else {
			session := response.GetSession()
			if session.Id == "" {
				log.Print("Got an empty session in response to login")
				getLoginPage(c, "Invalid (empty) session returned")
			} else {
				var cookie fasthttp.Cookie
				cookie.SetKey("sessionid")
				cookie.SetValue(session.Id)
				cookie.SetExpire(time.Unix(session.Expires, 0))
				cookie.SetHTTPOnly(true)
				c.Response.Header.SetCookie(&cookie)
				c.Set("session", session) // set new session in context, in case it's used one day
				c.SetFlash("msg", "You are now logged in")
				c.Redirect("/")
			}
		}
	} else {
		getLoginPage(c, "")
	}
}

func getLoginPage(c *iris.Context, msg string) {
	if msg != "" {
		c.Set("flashmsg", msg)
	}
	c.MustRender("login.html", NewViewData(c))
}

func HandleLogout(c *iris.Context) {
	s := c.Get("session").(*model.Session)

	if _, err := caller.Logout(s.Id); err != nil {
		log.Printf("Error logging out: %s", err.Error())
		if response2, err2 := caller.Session(s.Id); err2 != nil {
			log.Printf("Error checking session to log out: %s", err2.Error())
			c.SetFlash("msg", "Sorry, an error occurred. Try again.")
			c.Redirect("/")
		} else {
			if s2 := response2.GetSession(); s2.Id != "" {
				// session was not removed
				c.SetFlash("msg", "Sorry, couldn't log you out. Try again.")
				c.Redirect("/")
			} else {
				// session was removed and the error was in the call itself
				postLogout(c)
			}
		}
	} else {
		postLogout(c)
	}
}

func postLogout(c *iris.Context) {
	c.SetCookieKV("sessionid", "")
	c.SetFlash("msg", "You've been logged out")
	c.Redirect("/login")
}
