package controllers

import (
	"time"

	"bitbucket.org/iminsoft/employee-cli/model"

	"github.com/kataras/iris"
)

type viewData struct {
	Username string
	Message  string
	Expires  string
}

func NewViewData(c *iris.Context) *viewData {
	var username, expires, flash string
	var err error

	if s := c.Get("session"); s != nil {
		session := s.(*model.Session)
		username = session.Username
		expires = time.Unix(session.Expires, 0).String()
	}

	if flash, err = c.GetFlash("msg"); flash == "" || err != nil {
		flash = c.GetString("flashmsg")
	}

	return &viewData{
		Username: username,
		Message:  flash,
		Expires:  expires,
	}
}

func HandleHome(c *iris.Context) {
	if vd := NewViewData(c); vd.Username != "" {
		c.Render("home.html", vd)
	} else {
		c.Redirect("/login")
	}
}
