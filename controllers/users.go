package controllers

import (
	"bytes"
	"encoding/base64"
	"log"
	"strings"
	"time"

	"bitbucket.org/iminsoft/employee-cli/model"
	"bitbucket.org/iminsoft/front/caller"
	"github.com/kataras/iris"
)

const pagesize = 50

type attribute struct {
	Name        string
	DisplayName string
	Editable    bool
}

type usersViewData struct {
	*viewData
	Employees []*model.Record
	Total     int64
	Count     int64
	Offset    int64
	PageSize  int64
}

type userViewData struct {
	*viewData
	Employee      *model.Record
	AttrToEdit    map[string]string
	AttrToDisplay map[string]string
	Created       time.Time
	Updated       time.Time
	Photo         string
}

var (
	attributes = []attribute{
		{"l", "Location", false},
		{"o", "Office", false},
		{"displayName", "Display name", false},
		{"manager", "Manager", false},
		{"mobile", "Mobile number", false},
		{"mail", "Email", false},
		{"xProfileType", "Profile type", false},
		{"xRadiusTunnelPrivateGroupID", "Radius tunnel private group ID", true},
		{"xSSHPublicKey", "SSH Public Key", true},
	}
)

func HandleUserlist(c *iris.Context) {
	offset, err := c.URLParamInt64("offset")
	if err != nil {
		log.Printf("error parsing %s: %s", c.URLParam("offset"), err.Error())
	}

	if response, err := caller.Users(pagesize, offset); err != nil {
		c.SetFlash("msg", err.Error())
		c.Redirect("/")
	} else {
		if response.Error != "" {
			c.SetFlash("msg", "Error in response: "+response.Error)
			c.Redirect("/")
		} else {
			vd := usersViewData{
				NewViewData(c),
				response.GetEmployees(),
				response.Total,
				int64(len(response.GetEmployees())),
				offset,
				pagesize,
			}
			c.Render("userlist.html", vd)
		}
	}
}

func HandleUser(c *iris.Context) {
	if response, err := caller.User(c.Param("userId"), ""); err != nil {
		c.SetFlash("msg", err.Error())
		c.Redirect("/")
	} else {
		var photo string
		attributesToDisplay := make(map[string]string)
		attributesToEdit := make(map[string]string)
		employee := response.GetEmployee()
		attributeValues := employee.GetAttributes()

		for _, a := range attributes {
			if a.Editable {
				if attr, ok := attributeValues[a.Name]; ok {
					attributesToEdit[a.DisplayName] = strings.Join(attr.Values, "")
				} else {
					if attr, ok = attributeValues[strings.ToLower(a.Name)]; ok {
						attributesToEdit[a.DisplayName] = strings.Join(attr.Values, "")
					} else {
						attributesToEdit[a.DisplayName] = ""
					}
				}
			} else {
				if attr, ok := attributeValues[a.Name]; ok {
					attributesToDisplay[a.DisplayName] = strings.Join(attr.Values, "")
				} else {
					if attr, ok = attributeValues[strings.ToLower(a.Name)]; ok {
						attributesToDisplay[a.DisplayName] = strings.Join(attr.Values, "")
					} else {
						attributesToDisplay[a.DisplayName] = ""
					}
				}
			}
		}
		if _, ok := attributeValues["photo"]; ok {
			photo = strings.Join(attributeValues["photo"].Values, "")
		}

		vd := userViewData{
			NewViewData(c),
			employee,
			attributesToEdit,
			attributesToDisplay,
			time.Unix(employee.Created, 0),
			time.Unix(employee.Updated, 0),
			photo,
		}
		c.Render("user.html", vd)
	}
}

func HandleUserSave(c *iris.Context) {
	empId := c.Param("userId")
	username, err := getUsername(c, empId)
	if err != nil {
		c.SetFlash("msg", err.Error())
		c.Redirect("/users/" + empId)
	}

	attr := make(map[string]string)
	for _, a := range attributes {
		if a.Editable {
			attr[a.Name] = c.FormValueString(a.DisplayName)
		}
	}

	// TODO: the username here will have to change to empId
	if response2, err2 := caller.ModifyAttributes(username, attr); err2 != nil {
		c.SetFlash("msg", err2.Error())
	} else {
		if response2.Error != "" {
			c.SetFlash("msg", response2.Error)
		} else {
			if response2.Info != "" {
				c.SetFlash("msg", response2.Info)
			} else {
				c.SetFlash("msg", "Attributes saved")
			}
		}
	}
	c.Redirect("/users/" + empId)
}

func HandleAvatarUpload(c *iris.Context) {
	empId := c.Param("userId")
	username, err := getUsername(c, empId)
	if err != nil {
		c.SetFlash("msg", err.Error())
		c.Redirect("/users/" + empId)
	}
	// TODO: prevent users from uploading avatars for other users
	info, err2 := c.FormFile("avatar")
	if err2 != nil {
		c.SetFlash("msg", err2.Error())
		c.Redirect("/users/" + empId)
		return
	}

	f, _ := info.Open()
	defer f.Close()
	var content bytes.Buffer
	content.ReadFrom(f)
	encoded := base64.StdEncoding.EncodeToString(content.Bytes())

	// TODO: the username here will have to change to empId
	if response, err3 := caller.ModifyPhoto(username, encoded); err3 != nil {
		c.SetFlash("msg", err3.Error())
	} else {
		if response.Error != "" {
			c.SetFlash("msg", response.Error)
		} else {
			if response.Info != "" {
				c.SetFlash("msg", response.Info)
			} else {
				c.SetFlash("msg", "Avatar successfully uploaded")
			}
		}
	}
	c.Redirect("/users/" + empId)
}

func getUsername(c *iris.Context, empId string) (string, error) {
	if s := c.Get("session").(*model.Session); empId == s.Id {
		return s.Username, nil
	}

	if response, err := caller.User(empId, ""); err != nil {
		return "", err
	} else {
		return response.GetEmployee().Username, nil
	}
}
