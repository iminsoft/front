package caller

import (
	"encoding/json"
	"errors"
	"fmt"

	"bitbucket.org/iminsoft/employee-cli/model"
	"github.com/micro/go-micro/client"
	e "github.com/micro/go-micro/errors"
	"github.com/micro/go-micro/metadata"
	"golang.org/x/net/context"
)

const serviceUrl = "im.employee.cli"

func Login(login, password, domain string) (*model.LoginResponse, error) {
	request := client.NewRequest(serviceUrl, "Employee.Login", &model.LoginRequest{login, login + "@" + domain, password})
	response := &model.LoginResponse{}

	err := call(request, response)

	return response, err
}

func Logout(sessionId string) (*model.LogoutResponse, error) {
	request := client.NewRequest(serviceUrl, "Employee.Logout", &model.LogoutRequest{sessionId})
	response := &model.LogoutResponse{}

	err := call(request, response)

	return response, err
}

func Users(limit, offset int64) (*model.SearchResponse, error) {
	request := client.NewRequest(serviceUrl, "Employee.Search", &model.SearchRequest{
		Limit:  limit,
		Offset: offset,
		Sort:   "username",
	})
	response := &model.SearchResponse{}

	err := call(request, response)

	return response, err
}

func User(id, username string) (*model.GetResponse, error) {
	request := client.NewRequest(serviceUrl, "Employee.Get", &model.GetRequest{id, username})
	response := &model.GetResponse{}

	err := call(request, response)

	return response, err
}

func Session(id string) (*model.SessionResponse, error) {
	request := client.NewRequest(serviceUrl, "Employee.Session", &model.SessionRequest{id})
	response := &model.SessionResponse{}

	err := call(request, response)

	return response, err
}

func ModifyPhoto(empid, v string) (*model.ModifyResponse, error) {
	value := &model.Attribute{[]string{v}}
	attr := make(map[string]*model.Attribute)
	attr["photo"] = value

	return modify(empid, attr)
}

func ModifyAttributes(empid string, kv map[string]string) (*model.ModifyResponse, error) {
	attr := make(map[string]*model.Attribute)
	for k, v := range kv {
		if v == "" {
			attr[k] = &model.Attribute{[]string{}}
		} else {
			attr[k] = &model.Attribute{[]string{v}}
		}
	}

	return modify(empid, attr)
}

func modify(empid string, attr map[string]*model.Attribute) (*model.ModifyResponse, error) {
	request := client.NewRequest(serviceUrl, "Employee.Modify", &model.ModifyRequest{empid, attr})
	response := &model.ModifyResponse{}

	err := call(request, response)

	return response, err
}

func call(request client.Request, response interface{}) error {
	context := metadata.NewContext(context.Background(), map[string]string{}) // maybe this could be taken from c?
	if err := client.Call(context, request, response); err != nil {
		var se e.Error
		if jsonErr := json.Unmarshal([]byte(err.Error()), &se); jsonErr != nil {
			return errors.New(fmt.Sprint("Error decoding JSON (%v): %v", err.Error(), jsonErr))
		}
		return errors.New("Error during call: (" + se.Status + ") " + se.Detail)

	}
	return nil
}
