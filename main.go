package main

import (
	"html/template"
	"log"
	"time"

	"bitbucket.org/iminsoft/front/caller"
	"bitbucket.org/iminsoft/front/controllers"
	"github.com/iris-contrib/middleware/logger"
	"github.com/iris-contrib/template/html"
	"github.com/kataras/iris"
)

func authMiddleware(c *iris.Context) {
	// session expiration was already checked inside "sessionGetter", so here
	// we only check its presence in context
	if s := c.Get("session"); s == nil {
		c.SetFlash("msg", "You need to be logged in to access this page")
		c.Redirect("/login")
	} else {
		c.Next()
	}
}

func sessionGetter(c *iris.Context) {
	if sid := c.GetCookie("sessionid"); sid != "" {
		if response, err := caller.Session(sid); err != nil {
			log.Printf("Error when getting session (%s) from DB: %s", sid, err.Error())
		} else {
			if s := response.GetSession(); s != nil {
				if s.Expires < time.Now().Unix() {
					caller.Logout(sid)
					c.SetCookieKV("sessionid", "")
					c.SetFlash("msg", "Your session has expired")
					c.Redirect("/login")
				}
				c.Set("session", s)
			}
		}
	}
	c.Next()
}

func main() {
	iris.UseTemplate(html.New(html.Config{
		Layout: "layout.html",
		Funcs: template.FuncMap{
			"minus": func(a, b int64) int64 {
				return a - b
			},
			"plus": func(a, b int64) int64 {
				return a + b
			},
		},
	}))

	iris.Use(logger.New(iris.Logger))
	iris.UseFunc(sessionGetter)

	iris.Get("/", controllers.HandleHome)
	iris.Any("/login", controllers.HandleLogin)
	iris.Get("/logout", authMiddleware, controllers.HandleLogout)
	iris.Static("/css", "./resources/css", 1)
	iris.Static("/js", "./resources/js", 1)

	users := iris.Party("/users")
	{
		users.UseFunc(authMiddleware)

		users.Get("/", controllers.HandleUserlist)
		users.Get("/:userId", controllers.HandleUser)
		users.Post("/save/:userId", controllers.HandleUserSave)
		users.Post("/avatarupload/:userId", controllers.HandleAvatarUpload)
	}

	iris.Listen(":8080")
}
